import './App.css';
import { Routes, Route, Navigate } from 'react-router-dom';
import { Home } from './component/Pages/Home';
import About from './component/Pages/About';
import Register from './component/Register';
import Login from './component/Login';
import 'devextreme/dist/css/dx.light.css';
import { PrivateRoute, AnonymousRoute } from './Service/Router';
import { Sidebar } from './component/Global/SideBar/Sidebar';
import Service from './Service/CallapiBackEnd';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import React, { useEffect, useState } from 'react';

function App() {
  const [sidebar, setSidebar] = useState(true);
  const showSidebar = () => setSidebar(!sidebar);

  return (
    <>
      <div className='contrainer'>
        {Service?.IsLogin() ? <Sidebar item={sidebar}></Sidebar> : <></>}
        <div className='main'>
          {/* header */}
          {Service.IsLogin() ? <div className='hearder'>
            <nav className='navbar'>
              {sidebar ? <div className='navicon' to='#'>
                <AiIcons.AiOutlineClose onClick={showSidebar} />
              </div> : <div className='navicon' to='#'>
                <FaIcons.FaBars onClick={showSidebar} />
              </div>
              }
              <div className='user-login'> 
              <AiIcons.AiOutlineUser style={{marginRight: "5px",fontSize:"26px"}}/>
              <p className='mail-login'>dungp0858@gmail.com</p> 
              </div>
            </nav>
          </div> : null}
          {/* end */}
          <div className="content">
            <Routes>
              <Route element={<PrivateRoute />}>
                {/* <Route path='/' element={<Navigate to="/home" />}></Route> */}
                <Route path='/home' element={<Home />}></Route>
                <Route path='/about' element={<About />}></Route>
              </Route>
              <Route element={<AnonymousRoute />}>
                <Route path='/' element={<Navigate to="/login" />}></Route>
                <Route path='/login' element={<Login />}></Route>
                <Route path='/register' element={<Register />}></Route>
              </Route>
            </Routes>
          </div>
          
        </div>
      </div>
    </>
  );
}

export default App;
