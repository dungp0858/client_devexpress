import React from 'react'
import CircleLoader  from "react-spinners/CircleLoader";
import "./Css/Loading.css";
export default function Loading(props) {
    return (
        <div className='cssloading'>
            <CircleLoader 
                color={"#d6368b"}
                loading={props.Loading}
                // cssOverride={override}
                size={50}
                aria-label="Loading..."
                data-testid="loader"
            />
        </div>

    )
}
