import React, { useState,useEffect } from 'react'
import "../Css/Register.css";
//import { useNavigate } from "react-router-dom"
import { ValidationRegister } from '../Validation';
import Service from '../Service/CallapiBackEnd';
// import Config from '../Service/Config';
const Login = () => {
 //   const dieuhuongRouter = useNavigate();
    const [DataRegister,setDataRegister] =useState({
        TEN_DANG_NHAP:'',
        MAT_KHAU:'',
        TEN_NGUOI_DUNG:'',
        EMAIL:''
    });
    const [errors, seterrors] = useState({});
    const [ccheck, setCheck] = useState(true);
    //Around funtion
    const handleInput = (e) =>{
        setDataRegister({ ...DataRegister, [e.target.name]: e.target.value });
    }

    useEffect(()=>{
        if(DataRegister.TEN_DANG_NHAP ==='' && DataRegister.MAT_KHAU === '' && DataRegister.TEN_NGUOI_DUNG === '' && DataRegister.EMAIL ===''){
          setCheck(false);
        }
        seterrors(ValidationRegister(DataRegister));
      },[DataRegister]);

    const PostLogin = (e) => {
        e.preventDefault(); 
        setCheck(true);
        if(Object.keys(errors).length === 0){
          // call api
          Service.Register('HeThong/NguoiDung/Create',DataRegister).then(
            (response) => {
            //   if(Config.checkReponStatus(response)){
               
            //   }
            //   history.push('/')
            //   Setloading(false);
            }
          );
          //alert("dgsg");
        }

    }
    return (
        <div className='page'>
            <div className='cover-register'>
                <h1>Đăng Ký</h1>
                <div className='formlogin'>
                    <div className='uer'>
                        <label htmlFor='username'>Tên đăng nhập:</label>
                        <input type='text' id='username' name='TEN_DANG_NHAP' onChange={handleInput} value={DataRegister.TEN_DANG_NHAP} placeholder='tên đăng nhập' />
                        {(ccheck=== true && errors.TEN_DANG_NHAP) && <p style={{ color: "red" }}>{errors.TEN_DANG_NHAP}</p>}
                    </div>
                    <div className='pass'>
                        <label htmlFor='password'>Mật khẩu:</label>
                        <input type='password' id='password'name='MAT_KHAU' onChange={handleInput} value={DataRegister.MAT_KHAU} placeholder='mật khẩu' />
                        {(ccheck=== true && errors.MAT_KHAU) && <p style={{ color: "red" }}>{errors.MAT_KHAU}</p>}
                    </div>

                    <div className='pass'>
                        <label htmlFor='Hovaten'>Họ và tên:</label>
                        <input type='text' id='Hovaten' name='TEN_NGUOI_DUNG' onChange={handleInput} value={DataRegister.TEN_NGUOI_DUNG} placeholder='Họ và tên' />
                        {(ccheck=== true && errors.TEN_NGUOI_DUNG) && <p style={{ color: "red" }}>{errors.TEN_NGUOI_DUNG}</p>}
                    </div>
                    <div className='pass'>
                        <label htmlFor='Email'>Email:</label>
                        <input type='email' id='Email' name='EMAIL' onChange={handleInput}  value={DataRegister.EMAIL} placeholder='Email' />
                        {(ccheck=== true && errors.EMAIL) && <p style={{ color: "red" }}>{errors.EMAIL}</p>}
                    </div>
                </div>
                <div className='login-btn' onClick={PostLogin}>Đăng ký</div>
            </div>
        </div>

    );
}
export default Login