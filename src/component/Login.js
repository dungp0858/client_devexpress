import React from 'react'
import "../Css/Login.css";
import { Link, useNavigate } from "react-router-dom";
import { useState, useEffect } from 'react';
import { Validation } from '../Validation';
import Service from '../Service/CallapiBackEnd';
import Loading from '../Loading';
import Config from '../Service/Config';
import Cookies from 'universal-cookie';
const Login = () => {
  const dieuhuongRouter = useNavigate();
  const cookies = new Cookies();
  const [data, setData] = useState({
    TEN_DANG_NHAP: '',
    MAT_KHAU: ''
  });
  const [errors, seterrors] = useState({});
  const [ccheck, setCheck] = useState(true);
  const [loading, Setloading] = useState(false);
  //Around funtion
  const handleInput = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  }

  
  useEffect(() => {
    if (data.TEN_DANG_NHAP === '' && data.MAT_KHAU === '') {
      setCheck(false);
    }
    seterrors(Validation(data));
  }, [data]);

  const PostLogin = (e) => {
    e.preventDefault();
    setCheck(true);
    if (Object.keys(errors).length === 0) {
      // call api
      Setloading(true);
      Service.Login("HeThong/dangnhap", data).then(
        (response) => {
          if(Config.checkReponStatus(response)){
            const res = response.data.Data;
            cookies.set('Token', res, { path: '/' });
            setTimeout(()=>{
              dieuhuongRouter('/home');
              window.location.reload(false);
            },150)
           
          }
          Setloading(false);
        }

      );
    }
  }
  return (
    <div className='page'>
      {loading === true && <Loading Loading={loading} />}
      <div className='cover'>
        <h1>Đăng nhập</h1>
        <div className='formlogin'>
          <div className='uer'>
            <label htmlFor='username'>Tên đăng nhập:</label>
            <input type='text' id='username' onChange={handleInput} value={data.TEN_DANG_NHAP} name='TEN_DANG_NHAP' placeholder='tên đăng nhập' />
            {(ccheck === true && errors.TEN_DANG_NHAP) && <p style={{ color: "red" }}>{errors.TEN_DANG_NHAP}</p>}
          </div>
          <div className='pass'>
            <label htmlFor='password'>Mật khẩu:</label>
            <input type='password' id='password' onChange={handleInput} value={data.MAT_KHAU} name='MAT_KHAU' placeholder='mật khẩu' />
            {(ccheck === true && errors.MAT_KHAU) && <p style={{ color: "red" }}>{errors.MAT_KHAU}</p>}
          </div>
        </div>
        <div className='login-btn' onClick={PostLogin} disabled>Đăng nhập</div>

        <div> <Link to='/register'>Đăng ký người dùng</Link></div>
      </div>
    </div>
  );
}
export default Login