import React,  { useCallback, useState,useRef } from 'react'
import Config from '../../Service/Config.js';
import CustomGrid from '../../Service/CustomGrid.js';
import { Link, useNavigate } from "react-router-dom";
import Api from '../../Service/CallapiBackEnd.js';
import 'devextreme/data/odata/store';
// import { customers } from '../../data.js';
// import DataGrid, { Column, Paging, Pager } from 'devextreme-react/data-grid';
// import CustomStore from 'devextreme/data/custom_store';
export const Home = (props) => {
  const dieuhuongRouter = useNavigate();
  const IsLogin = Api.IsLogin();
    // đăng xuất
  const PostLogin = (e) => {
    e.preventDefault();
    Api.RemoveToken();
    dieuhuongRouter('/login');
    window.location.reload(false);

  };

  const Col = [
   {"df":"MA_NGUOI_DUNG","c":"Mã người dùng"},
   {"df":"TEN_NGUOI_DUNG","c":"Tên người dùng"},
   {"df":"TEN_DANG_NHAP","c":"Tên đăng nhập"},
   {"df":"EMAIL","c":"Email"}
  ];
  var dataMain = Config.DataSource("HeThong/NguoiDung", ["ID"], ["ID", "MA_NGUOI_DUNG", "TEN_NGUOI_DUNG", "TEN_DANG_NHAP", "EMAIL"], ["ID"]);
   const onInitNewRow = (e)=>{
    e.data.MA_NGUOI_DUNG = "Dũng Phạm";
    e.data.TEN_NGUOI_DUNG = "Dũng";
   };
  return (
    <div className='content-page'>
      <div className='login-btn' onClick={PostLogin} disabled>Đăng xuất</div>
      <CustomGrid props={{dataSource:dataMain,col:Col,onInitNewRow:onInitNewRow}}/>
    </div>
  )

}
