import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Sidebar.css';


const SubMenu = ({ item }) => {
  const [subnav, setSubnav] = useState(false);

  const showSubnav = () => setSubnav(!subnav);
  return (
    <>
      <Link className='sidebarlink' to={item.path} onClick={item.subNav && showSubnav}>
        <div style={{display:"flex"}}>
          {item.icon}
          <label className='sidebarlable' style={{display: item.sidebar?"block":"none"}}>{item.title}</label>
        </div>
        <div>
          {item.subNav && subnav ? item.iconOpened: item.subNav? item.iconClosed: null}
        </div>
      </Link>
      {subnav &&
        item.subNav.map((item, index) => {
          return (
            <Link className="dropdownlink" to={item.path} key={index}>
              {item.icon}
              <label className='sidebarlable'>{item.title}</label>
            </Link>
          );
        })}
    </>
  );
};

export default SubMenu;
