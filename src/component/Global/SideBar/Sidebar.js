import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
// import { Link } from 'react-router-dom';
import { SidebarData } from './SidebarData';
import { IconContext } from 'react-icons/lib';
import SubMenu from './SubMenu';
import './Sidebar.css';
export const Sidebar = ({ item }) => {
    // console.log(item);
    const [sidebar, setSidebar] = useState(item);
    useEffect(()=>{
        setSidebar(item);
    });
  
    return (
        <>
            <div className='menu'>
                <div className='sidebar' style={{ width: sidebar ? "250px" : "76px" }}>
                    <IconContext.Provider value={{ color: '#fff' }}>
                        {/* <div className="sidebarnav" style={{ display: sidebar ? "none" : "block" }} > */}
                        <div className="sidebarwrap">
                            {SidebarData.map((items, index) => {
                                const i = Object.assign(items, { sidebar: sidebar });
                                return <SubMenu item={i} key={index} />;
                            })}
                        </div>
                        {/* </div> */}
                    </IconContext.Provider>
                </div>

            </div>
        </>
    )

}
