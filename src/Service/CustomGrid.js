import React from 'react';
import DataGrid, { Column, Paging, Pager,Selection,Editing, Popup,Form,DataGridTypes} from 'devextreme-react/data-grid';
import Config from '../Service/Config.js';
function CustomGrid(props) {
    
    const allowedPageSizes = [8, 12, 20];
    const addColumn = (c,op)=>{
        const colk = [];
       if(c != null){
            c.forEach((e,index)=>{
                 colk.push(<Column key={index} dataField = {e['df']} caption={e['c']}></Column>)
            })
        }
        return colk;
    }
    return (
        <div>
            <DataGrid
                dataSource={props.props.dataSource}
                showBorders={true}
                remoteOperations={true}
                keyExpr="ID"
                onInitNewRow={props.props.onInitNewRow}
            >
                {addColumn(props.props.col)}

                <Paging defaultPageSize={10} />
                <Pager
                    showPageSizeSelector={true}
                    allowedPageSizes={allowedPageSizes}
                />
                <Selection mode="multiple" />
                {/* edit */}
                <Editing mode="popup" allowUpdating={true} allowAdding={true} allowDeleting={true}>
                    <Popup title="Người dùng" showTitle={true} width={700} height={525} />
                </Editing>
            </DataGrid>
        </div>

    )
}
export default CustomGrid