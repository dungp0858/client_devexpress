import axios, { AxiosHeaders } from 'axios';
import jwt from 'jwt-decode';
import React,{Children, createContext, useEffect, useState}  from 'react';
import Cookies from 'universal-cookie';
const API_URL ="https://localhost:7249/api/";
const Cookie = new Cookies();

const GetAuth=()=>{
  var headers={};
  const tokens = Cookie.get('Token');
   if(typeof tokens != "undefined"){
    headers = {
       Authorization: `Bearer ${tokens.accessToken}`,
    };
  }
  return headers;
}
var headers=  GetAuth();
// Login 
const Login = (url,data) =>{
    return axios.post(API_URL + url,data).then((response)=>{
      return response;
    });

};
const Register=(url,data)=>{
  return axios.post(API_URL+url,data,{headers}).then(response=>{
   return response;
  });
}
const getData =(url,data={})=>{
  return axios.get(API_URL+url,{params:data,headers}).then(response=>{
    return response;
   });
}
//token 
const Gettoken = ()=>{
  const[token, setToken] = useState(null);
  useEffect(()=>{
    const tokens = Cookie.get('Token');
    if(typeof tokens != "undefined"){
      setToken(tokens.accessToken);
    }
  },[]);
 return token;
}
const RemoveToken = ()=>{
  return Cookie.remove('Token');
};

const IsLogin = ()=>{
  if(Gettoken() != null){
    return true;
  }else{
    return false;
  }

}
const Service = {
Login,
Gettoken,
Register,
getData,
RemoveToken,
IsLogin
}
export default Service;
// const CallapiBackEnd