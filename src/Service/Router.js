import Service from "./CallapiBackEnd";
import {Navigate,Outlet } from 'react-router-dom';
import Cookies from 'universal-cookie';

const Cookie = new Cookies();
const PrivateRoute =()=>{
    const tokens = Cookie.get('Token');
    if(typeof tokens !== "undefined"){
        console.log(tokens);
        return tokens.accessToken !== ""? <Outlet/> : <Navigate to="/login" replace/>
    }else{
        return <Navigate to="/login" replace/>
    }

}
const  AnonymousRoute = () =>{
const IsLogin = Service.IsLogin();
  return IsLogin ? <Navigate to="/home" replace /> : <Outlet />;
}
export {
    PrivateRoute,
    AnonymousRoute
};
