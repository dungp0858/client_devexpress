import { toHaveDisplayValue } from "@testing-library/jest-dom/matchers";
import CustomStore from 'devextreme/data/custom_store';
import Api from '../Service/CallapiBackEnd.js';
import { Item } from 'devextreme-react/form';
import React,  { useCallback, useState,useRef } from 'react'

const checkReponStatus=(response,refresh=true)=>{
var result = true;
if(response.data.State === -1){
    result = false;
    alert(response.data.Msg);
}else if (response.status === 401) {
    result = false;
    alert("Không có quyền truy cập");
} else if (response.status === 404) {
    result = false;
    alert("Đường dẫn sai");
} else if (response.status === 500) {
    result = false;
    alert("dddd");
    //setTimeout(() => {
    //    if (refresh) window.location.reload();
    //}, 3000);
} else {
    var data = response.data;
    if (data.State === 0) {
        result = false;
        alert("không có quyền");
    } else if (data.State === -1) {
        result = false;
        alert(response.data.Msg, "error");
        //setTimeout(() => {
        //window.location.reload();
        //}, 3000);
    } else if (data.State === 2) {
        result = false;
        alert(response.data.Msg);
        //setTimeout(() => {
        //window.location.reload();
        //}, 3000);
    }
}
return result;
}
// grid
const OnLoad = (loadOptions, url, fields, keys, defaultSort, exParamerter) => {
    //console.log(loadOptions);
    var parameters = {};
    parameters.fields = JSON.stringify(fields);
    parameters.keys = JSON.stringify(keys);
    parameters.defaultSort = JSON.stringify(defaultSort);
    parameters.sort = loadOptions.sort ? JSON.stringify(loadOptions.sort) : null;
    parameters.filter = loadOptions.filter ? JSON.stringify(loadOptions.filter) : null;
    parameters.skip = loadOptions.skip;
    parameters.take = loadOptions.take;
    parameters.dataField = loadOptions.dataField;
    parameters.searchOperation = loadOptions.searchOperation ? loadOptions.searchOperation : null;
    parameters.searchValue = loadOptions.searchValue ? loadOptions.searchValue : null;
    parameters.requireTotalCount = loadOptions.requireTotalCount || true;
    Object.assign(parameters, exParamerter || {});
    return Api.getData(url, parameters).then((response) => {
      if (Config.checkReponStatus(response)) {
        var data = response.data.Data;
        return { data: data.items, totalCount: data.totalCount };
      }
      return Promise.reject();
    }, (response) => {
      if (!Config.checkReponStatus(response)) return Promise.reject("Lỗi tải dữ liệu");
    });
  };
const DataSource = (u, k, f, s, op = {}) => {
    op = Object.assign({
      ul: "/List",
      ulo: function () { return null },
      bl: function () { },
      al: function (response) { },
      uu: "",
      uuo: function () { return null },
      bu: function (key, values) { },
      au: function (response) { },
      ui: "/Create",
      uio: function () { return null },
      bi: function (values) { },
      ai: function (response) { },
      ca: true,
      bk: null,
    }, op);
    var data = new CustomStore({
      key: k.length === 1 ? k[0] : k,
      load(loadOptions) {
        var result = OnLoad(loadOptions, u + op.ul, f, k, s, op.ulo());
        result.then(function (response) {
          data.items = response.data;
          op.al(response);
        });
        return result;
      },
      byKey: op.bk,
      cacheRawData: op.ca,
    });
    return data;
}

const colMap = {
    df: "dataField",
    c: "caption",
    ml: "editorOptions.maxLength",
    ficc: "formItem.cssClass",
    cp: "formItem.colSpan",
    dt: "dataType",
    f: "format",
    v: "visible",
    vi: "visibleIndex",
    fv: "formItem.visible",
    w: "width",
    ft: "falseText",
    tt: "trueText",
    af: "allowFiltering",
    as: "allowSorting",
    d: "editorOptions.disabled",
    ro: "editorOptions.readOnly",
    n: "name",
    fn: "formItem.name",
    en: "editorOptions.name",
    cc: "cssClass",
    a: "alignment",
    ae: "allowEditing",
    gi: "groupIndex",
    ef: "editorOptions.format",
    edf: "editorOptions.displayFormat",
    eov: "editorOptions.value",
    fx: "fixed",
    fxp: "fixedPosition",
    ect: "editCellTemplate",
    ct: "cellTemplate",
    edph: "editorOptions.placeholder",
    scv: 'setCellValue',
    si: 'sortIndex',
    so: 'sortOrder',
    scc: 'showInColumnChooser',
};
const Config ={
    checkReponStatus,
    DataSource,
}

export default Config;