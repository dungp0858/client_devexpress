
//Validation Đăng nhập
export  function Validation(values){
const errors = {};
//const password_pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if(values.MAT_KHAU === ''){
        errors.MAT_KHAU = "Mật khẩu không được để trống";
    }
    // else if(!password_pattern.test(values.MAT_KHAU))
    // {
    //     errors.MAT_KHAU = "Mật khẩu phải chứa ít nhất 1 chữ hoa,thường,1 số và 1 ký tự đặc biệt";
    // }
    if(values.TEN_DANG_NHAP ===''){
        errors.TEN_DANG_NHAP = 'Tên đăng nhập không được để trống';
    }
    return errors;
}
//Validation Đăng ký
export function ValidationRegister(values){
    const errors = {};
    const password_pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    const email_pattern = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    if(values.TEN_NGUOI_DUNG ===''){
        errors.TEN_NGUOI_DUNG = 'Tên người dùng không được để trống'
    }
    if(values.EMAIL===''){
        errors.EMAIL = 'Email không được để trống'
    }else if(!email_pattern.test(values.EMAIL)){
        errors.EMAIL = 'Email không đúng định dạng'
    }
    if(values.MAT_KHAU === ''){
        errors.MAT_KHAU = "Mật khẩu không được để trống";
    }else if(!password_pattern.test(values.MAT_KHAU))
    {
        errors.MAT_KHAU = "Mật khẩu phải chứa ít nhất 1 chữ hoa,thường,1 số và 1 ký tự đặc biệt";
    }
    if(values.TEN_DANG_NHAP ===''){
        errors.TEN_DANG_NHAP = 'Tên đăng nhập không được để trống';
    }
    return errors;
}
